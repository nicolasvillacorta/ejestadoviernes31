package com.captton.gobierno.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.gobierno.model.Banco;
import com.captton.gobierno.model.Empleado;
import com.captton.gobierno.model.Empresa;
import com.captton.gobierno.model.InputCambioDom;
import com.captton.gobierno.model.InputEmpleado;
import com.captton.gobierno.model.InputEmpresa;
import com.captton.gobierno.repository.BancoDao;
import com.captton.gobierno.repository.EmpleadoDao;
import com.captton.gobierno.repository.EmpresaDao;

@RestController
@RequestMapping({"/estado"})
public class MainController {

	@Autowired
	private BancoDao daobanco;
	@Autowired
	private EmpleadoDao daoempleado;
	@Autowired
	private EmpresaDao daoempresa;
	
	//1) Dar de alta un banco.
	@PostMapping(path= {"/banco"})
	public Banco create(@RequestBody Banco banco) {
		return daobanco.save(banco);
	}
	
	//2) Dar de alta una empresa y asignarle un banco en el momento del alta. (pasarle el ID de clave primaria del banco).
	@PostMapping(path= {"/empresa"})
	public ResponseEntity<Object> create(@RequestBody InputEmpresa ingresoDeEmpresa) {
		
		Banco bn = daobanco.findById(ingresoDeEmpresa.getIdBanco()).orElse(null);
		Empresa nuevaE = new Empresa();
		JSONObject obj = new JSONObject();
		
		if(bn!=null) {
			//nombre, dir, localidad, descr. Y BANCO
			nuevaE.setBanco(bn);
			nuevaE.setNombre(ingresoDeEmpresa.getEmpresa().getNombre());
			nuevaE.setDireccion(ingresoDeEmpresa.getEmpresa().getDireccion());
			nuevaE.setLocalidad(ingresoDeEmpresa.getEmpresa().getLocalidad());
			nuevaE.setDescripcion(ingresoDeEmpresa.getEmpresa().getDescripcion());
			
			Empresa aux = daoempresa.save(nuevaE);
			
			obj.put("error", 0);
			obj.put("message", "Creada nueva empresa con ID: "+aux.getId());
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", 1);
			obj.put("message", "No se encontro el banco buscado.");
			return ResponseEntity.ok().body(obj.toString());
		}

	}
	
	//3) Dar de alta un empleado y asignarle una empresa en el momento del alta.(pasarle el ID de clave primaria de la empresa)
	@PostMapping(path= {"/empleado"})
	public ResponseEntity<Object> create(@RequestBody InputEmpleado ingresoDeEmpleado){
		
		Empresa emp = daoempresa.findById(ingresoDeEmpleado.getIdEmpresa()).orElse(null);
		Empleado nuevoEmp = new Empleado();
		JSONObject obj = new JSONObject();
		
		if(emp!=null) {
			
			nuevoEmp.setEmpresa(emp);
			nuevoEmp.setNombre(ingresoDeEmpleado.getEmpleado().getNombre());
			nuevoEmp.setApellido(ingresoDeEmpleado.getEmpleado().getApellido());
			nuevoEmp.setDni(ingresoDeEmpleado.getEmpleado().getDni());
			nuevoEmp.setDireccion(ingresoDeEmpleado.getEmpleado().getDireccion());
			
			Empleado aux = daoempleado.save(nuevoEmp);
			
			obj.put("error", 0);
			obj.put("message", "Se añadio el empleado "+aux.getNombre()+" "+aux.getApellido()+" a la base de datos.");
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", 1);
			obj.put("message", "No se encontro la empresa buscada.");
			
			return ResponseEntity.ok().body(obj.toString());
		}
		
		
	}

	//4) Los empleados se pueden mudar cambiando su dirección pero la API recibe el DNI del empleado solamente como parametro de entrada.
	@PutMapping(path= {"/empleado"})
	public ResponseEntity<Object> update(@RequestBody InputCambioDom dniYdireccion){
		
		JSONObject obj = new JSONObject();
		List<Empleado> empleadosFound = daoempleado.findByDni(dniYdireccion.getDni());
		if(!empleadosFound.isEmpty()) {

			for(Empleado aux: empleadosFound) {	
				aux.setDireccion(dniYdireccion.getNuevaDireccion());
				daoempleado.save(aux);
			}
			
			obj.put("error", 0);
			obj.put("result", "Los domicilios fueron cambiados con exito.");
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", 1);
			obj.put("result", "No se encontro el empleado");
			return ResponseEntity.ok().body(obj.toString());
		}
	}

	//5) Se requiere un listado de todos los empleados de una empresa, la API recibe el ID de clave primaria de la empresas y la salida JSON debe tener los datos de la empresa consultada y el listado de los empleados.
	@GetMapping(path= {"/listado/{id}"})
	public ResponseEntity<Object> getListado(@PathVariable Long id){
		
		Empresa emp = daoempresa.findById(id).orElse(null);
		List<Empleado> empleadosFound = daoempleado.findByEmpresa(emp);
		
		JSONArray array = new JSONArray();
		JSONObject aux = new JSONObject();
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		
		if(!empleadosFound.isEmpty()) {
			aux.put("Nombre de la empresa", emp.getNombre());
			aux.put("Direccion", emp.getDireccion());
			aux.put("Localidad", emp.getLocalidad());
			aux.put("Descripcion", emp.getDescripcion());
			
			for(Empleado empAux: empleadosFound) {		
				JSONObject aux2 = new JSONObject();
				aux2.put("empleado", empAux.getNombre());
				array.put(aux2);
			}
			obj.put("Datos de la empresa", aux);
			obj.put("Empleados", array);
			return ResponseEntity.ok().body(obj.toString());
		}else {
			aux.put("error", 0);
			aux.put("message", "La empresa no tiene empleados y/o no existe.");	
			return ResponseEntity.ok().body(aux.toString());
		}
	}

}
