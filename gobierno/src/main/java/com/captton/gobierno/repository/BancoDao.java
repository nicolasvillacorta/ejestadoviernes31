package com.captton.gobierno.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.gobierno.model.Banco;

public interface BancoDao extends JpaRepository<Banco, Long> {

}
