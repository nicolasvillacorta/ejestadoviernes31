package com.captton.gobierno.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.gobierno.model.Empresa;

public interface EmpresaDao extends JpaRepository<Empresa, Long> {

}
