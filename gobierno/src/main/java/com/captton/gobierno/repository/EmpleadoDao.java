package com.captton.gobierno.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.gobierno.model.Empleado;
import com.captton.gobierno.model.Empresa;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {

	public List<Empleado> findByDni(String dni);
	
	public List<Empleado> findByEmpresa(Empresa empresa);
}
