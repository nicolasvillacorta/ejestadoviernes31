package com.captton.gobierno.model;

public class InputCambioDom {

	private String dni;
	private String nuevaDireccion;

	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNuevaDireccion() {
		return nuevaDireccion;
	}
	public void setNuevaDireccion(String nuevaDireccion) {
		this.nuevaDireccion = nuevaDireccion;
	}	
	
}
