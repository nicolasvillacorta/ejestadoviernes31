package com.captton.gobierno.model;

public class InputEmpleado {

	private Long idEmpresa;
	private Empleado empleado;
	
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empelado) {
		this.empleado = empelado;
	}
	
}
